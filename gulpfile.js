var gulp = require('gulp'),
    mainBowerFiles = require('main-bower-files'),
    concat = require('gulp-concat'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    bower = require('bower');

gulp.task('bower', function (cb) {
    bower.commands.install([], {save: true}, {})
        .on('end', function (installed) {
            console.log('Bower ready');
            cb();
        });
});

// gulp.task('libs', ['bower'], function () {
//     return gulp.src(mainBowerFiles())
//         .pipe(uglify())
//         .pipe(concat('libs.js'))
//         .pipe(gulp.dest('./static/js/'));
// });

// gulp.task('js', ['bower'], function () {
//     return gulp.src(['assets/js/panel.js', 'assets/**/*.js'])
//         // .pipe(uglify())
//         .pipe(concat('app.js'))
//         .pipe(gulp.dest('static/js/'));
// });

// gulp.task('tmpl', function () {
//     return gulp.src('assets/tmpl/**/*.tpl')
//         .pipe(template({
//             strict: true,
//             namespace: 'Templates',
//             name: function (file) {
//                 return file.relative.replace('.tpl', '');
//             }
//         }))
//         .pipe(uglify())
//         .pipe(concat('templates.js'))
//         .pipe(gulp.dest('static/js/'));
// });

gulp.task('sass', ['bower'], function () {
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./src/assets/css'));
});

gulp.task('sass-prod', ['bower'], function () {
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sass())
        .pipe(prefixer())
        .pipe(minify())
        .pipe(gulp.dest('./src/assets/css'));
});

// gulp.task('fonts', ['bower'], function () {
//     gulp.src(['./bower_components/**/fonts/**/*.{ttf,woff,woff2,eof,svg}'],
//         {base: './bower_components/'})
//         .pipe(gulp.dest('./static/fonts'));
// });

gulp.task('watch', function () {
    // gulp.watch('assets/tmpl/**/*.tpl', ['tmpl']);
    // gulp.watch('assets/js/**/*.js', ['js']);
    gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
});

gulp.task('build', ['sass-prod']);

gulp.task('default', ['sass', 'watch']);