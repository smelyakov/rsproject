// Garden Gnome Software - Skin
// Pano2VR pro 4.5.0/10633
// Filename: 55_my_controller_ko4egarka 140909.ggsk
// Generated Пн 20. окт 21:43:24 2014

function pano2vrSkin(player,base) {
	var me=this;
	var flag=false;
	var nodeMarker=new Array();
	var activeNodeMarker=new Array();
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=new Array();
	this.elementMouseOver=new Array();
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	for(i=0;i<prefixes.length;i++) {
		if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
			cssPrefix='-' + prefixes[i].toLowerCase() + '-';
			domTransition=prefixes[i] + 'Transition';
			domTransform=prefixes[i] + 'Transform';
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=new Array();
		stack.push(startElement);
		while(stack.length>0) {
			e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=new Array();
		var stack=new Array();
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.preloadImages=function() {
		var preLoadImg=new Image();
		preLoadImg.src=basePath + 'images/map_tigle__a.png';
		preLoadImg.src=basePath + 'images/autorotate__a.png';
		preLoadImg.src=basePath + 'images/down__a.png';
		preLoadImg.src=basePath + 'images/up__a.png';
		preLoadImg.src=basePath + 'images/right__a.png';
		preLoadImg.src=basePath + 'images/left__a.png';
		preLoadImg.src=basePath + 'images/zoom_out__a.png';
		preLoadImg.src=basePath + 'images/zoom_in__a.png';
		preLoadImg.src=basePath + 'images/fullscreen__a.png';
	}
	
	this.addSkin=function() {
		this._container_main_control=document.createElement('div');
		this._container_main_control.ggId="Container main control";
		this._container_main_control.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_main_control.ggVisible=true;
		this._container_main_control.className='ggskin ggskin_container';
		this._container_main_control.ggType='container';
		this._container_main_control.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				h=this.parentNode.offsetHeight;
				this.style.top=(-39 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -1px;';
		hs+='top:  -39px;';
		hs+='width: 642px;';
		hs+='height: 50px;';
		hs+=cssPrefix + 'transform-origin: 50% 100%;';
		hs+='opacity: 0.8;';
		hs+='visibility: inherit;';
		this._container_main_control.setAttribute('style',hs);
		this._container_main_control.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._container_main_control.style[domTransition]='none';
			} else {
				me._container_main_control.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._container_main_control.style.opacity='0.8';
			me._container_main_control.style.visibility=me._container_main_control.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._logo_55.style[domTransition]='none';
			} else {
				me._logo_55.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_55.style.opacity='0.8';
			me._logo_55.style.visibility=me._logo_55.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps.style[domTransition]='none';
			} else {
				me._maps.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps.style.opacity='1';
			me._maps.style.visibility=me._maps.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._logo_py6kin.style[domTransition]='none';
			} else {
				me._logo_py6kin.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_py6kin.style.opacity='1';
			me._logo_py6kin.style.visibility=me._logo_py6kin.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps_fon.style[domTransition]='none';
			} else {
				me._maps_fon.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps_fon.style.opacity='0.4';
			me._maps_fon.style.visibility=me._maps_fon.ggVisible?'inherit':'hidden';
		}
		this._container_main_control.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._container_main_control.style[domTransition]='none';
			} else {
				me._container_main_control.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._container_main_control.style.opacity='0.2';
			me._container_main_control.style.visibility=me._container_main_control.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._logo_55.style[domTransition]='none';
			} else {
				me._logo_55.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_55.style.opacity='0.2';
			me._logo_55.style.visibility=me._logo_55.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps.style[domTransition]='none';
			} else {
				me._maps.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps.style.opacity='0.5';
			me._maps.style.visibility=me._maps.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._logo_py6kin.style[domTransition]='none';
			} else {
				me._logo_py6kin.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_py6kin.style.opacity='0.5';
			me._logo_py6kin.style.visibility=me._logo_py6kin.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps_fon.style[domTransition]='none';
			} else {
				me._maps_fon.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps_fon.style.opacity='0';
			me._maps_fon.style.visibility='hidden';
		}
		this._line=document.createElement('div');
		this._line.ggId="line";
		this._line.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._line.ggVisible=true;
		this._line.className='ggskin ggskin_rectangle';
		this._line.ggType='rectangle';
		hs ='position:absolute;';
		hs+='left: -12px;';
		hs+='top:  0px;';
		hs+='width: 359px;';
		hs+='height: 49px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='background: #c8aa78;';
		hs+='border: 1px solid #c8aa78;';
		hs+='border-radius: 13px;';
		hs+=cssPrefix + 'border-radius: 13px;';
		this._line.setAttribute('style',hs);
		this._container_main_control.appendChild(this._line);
		this._map_tigle=document.createElement('div');
		this._map_tigle.ggId="map_tigle";
		this._map_tigle.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._map_tigle.ggVisible=true;
		this._map_tigle.className='ggskin ggskin_button';
		this._map_tigle.ggType='button';
		hs ='position:absolute;';
		hs+='left: 277px;';
		hs+='top:  11px;';
		hs+='width: 58px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._map_tigle.setAttribute('style',hs);
		this._map_tigle__img=document.createElement('img');
		this._map_tigle__img.className='ggskin ggskin_button';
		this._map_tigle__img.setAttribute('src',basePath + 'images/map_tigle.png');
		this._map_tigle__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._map_tigle__img.className='ggskin ggskin_button';
		this._map_tigle__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._map_tigle__img);
		this._map_tigle.appendChild(this._map_tigle__img);
		this._map_tigle.onclick=function () {
			flag=me._container_maps.ggPositonActive;
			if (me.player.transitionsDisabled) {
				me._container_maps.style[domTransition]='none';
			} else {
				me._container_maps.style[domTransition]='all 1000ms ease-out 0ms';
			}
			if (flag) {
				me._container_maps.ggParameter.rx=0;me._container_maps.ggParameter.ry=0;
				me._container_maps.style[domTransform]=parameterToTransform(me._container_maps.ggParameter);
			} else {
				me._container_maps.ggParameter.rx=-360;me._container_maps.ggParameter.ry=0;
				me._container_maps.style[domTransform]=parameterToTransform(me._container_maps.ggParameter);
			}
			me._container_maps.ggPositonActive=!flag;
		}
		this._map_tigle.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._map_tigle.style[domTransition]='none';
			} else {
				me._map_tigle.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._map_tigle.ggParameter.rx=0;me._map_tigle.ggParameter.ry=-2;
			me._map_tigle.style[domTransform]=parameterToTransform(me._map_tigle.ggParameter);
			me._txt_map_tigle.style[domTransition]='none';
			me._txt_map_tigle.style.opacity='1';
			me._txt_map_tigle.style.visibility=me._txt_map_tigle.ggVisible?'inherit':'hidden';
		}
		this._map_tigle.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._map_tigle.style[domTransition]='none';
			} else {
				me._map_tigle.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._map_tigle.ggParameter.rx=0;me._map_tigle.ggParameter.ry=0;
			me._map_tigle.style[domTransform]=parameterToTransform(me._map_tigle.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_map_tigle.style[domTransition]='none';
			} else {
				me._txt_map_tigle.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_map_tigle.style.opacity='0';
			me._txt_map_tigle.style.visibility='hidden';
		}
		this._map_tigle.onmousedown=function () {
			me._map_tigle__img.src=basePath + 'images/map_tigle__a.png';
		}
		this._map_tigle.onmouseup=function () {
			me._map_tigle__img.src=basePath + 'images/map_tigle.png';
		}
		this._txt_map_tigle=document.createElement('div');
		this._txt_map_tigle__text=document.createElement('div');
		this._txt_map_tigle.className='ggskin ggskin_textdiv';
		this._txt_map_tigle.ggTextDiv=this._txt_map_tigle__text;
		this._txt_map_tigle.ggId="txt map_tigle";
		this._txt_map_tigle.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_map_tigle.ggVisible=true;
		this._txt_map_tigle.className='ggskin ggskin_text';
		this._txt_map_tigle.ggType='text';
		this._txt_map_tigle.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (168-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: -105px;';
		hs+='top:  -32px;';
		hs+='width: 168px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_map_tigle.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: right;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_map_tigle__text.setAttribute('style',hs);
		this._txt_map_tigle__text.innerHTML="\u041f\u043b\u0430\u043d \u041a\u041b\u0423\u0411\u0410 \u0421\u043a\u0440\u044b\u0442\u044c\/\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c";
		this._txt_map_tigle.appendChild(this._txt_map_tigle__text);
		this._map_tigle.appendChild(this._txt_map_tigle);
		this._container_main_control.appendChild(this._map_tigle);
		this._autorotate=document.createElement('div');
		this._autorotate.ggId="autorotate";
		this._autorotate.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._autorotate.ggVisible=true;
		this._autorotate.className='ggskin ggskin_button';
		this._autorotate.ggType='button';
		hs ='position:absolute;';
		hs+='left: 237px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._autorotate.setAttribute('style',hs);
		this._autorotate__img=document.createElement('img');
		this._autorotate__img.className='ggskin ggskin_button';
		this._autorotate__img.setAttribute('src',basePath + 'images/autorotate.png');
		this._autorotate__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._autorotate__img.className='ggskin ggskin_button';
		this._autorotate__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._autorotate__img);
		this._autorotate.appendChild(this._autorotate__img);
		this._autorotate.onclick=function () {
			me.player.toggleAutorotate();
		}
		this._autorotate.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._autorotate.style[domTransition]='none';
			} else {
				me._autorotate.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._autorotate.ggParameter.rx=0;me._autorotate.ggParameter.ry=-2;
			me._autorotate.style[domTransform]=parameterToTransform(me._autorotate.ggParameter);
			me._txt_autorotate.style[domTransition]='none';
			me._txt_autorotate.style.opacity='1';
			me._txt_autorotate.style.visibility=me._txt_autorotate.ggVisible?'inherit':'hidden';
		}
		this._autorotate.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._autorotate.style[domTransition]='none';
			} else {
				me._autorotate.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._autorotate.ggParameter.rx=0;me._autorotate.ggParameter.ry=0;
			me._autorotate.style[domTransform]=parameterToTransform(me._autorotate.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_autorotate.style[domTransition]='none';
			} else {
				me._txt_autorotate.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_autorotate.style.opacity='0';
			me._txt_autorotate.style.visibility='hidden';
		}
		this._autorotate.onmousedown=function () {
			me._autorotate__img.src=basePath + 'images/autorotate__a.png';
		}
		this._autorotate.onmouseup=function () {
			me._autorotate__img.src=basePath + 'images/autorotate.png';
		}
		this._txt_autorotate=document.createElement('div');
		this._txt_autorotate__text=document.createElement('div');
		this._txt_autorotate.className='ggskin ggskin_textdiv';
		this._txt_autorotate.ggTextDiv=this._txt_autorotate__text;
		this._txt_autorotate.ggId="txt autorotate";
		this._txt_autorotate.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_autorotate.ggVisible=true;
		this._txt_autorotate.className='ggskin ggskin_text';
		this._txt_autorotate.ggType='text';
		this._txt_autorotate.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (163-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: -53px;';
		hs+='top:  -24px;';
		hs+='width: 163px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_autorotate.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: right;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_autorotate__text.setAttribute('style',hs);
		this._txt_autorotate__text.innerHTML="\u0410\u0432\u0442\u043e\u0432\u0440\u0430\u0449\u0435\u043d\u0438\u0435 \u0412\u043a\u043b\/\u0412\u044b\u043a\u043b";
		this._txt_autorotate.appendChild(this._txt_autorotate__text);
		this._autorotate.appendChild(this._txt_autorotate);
		this._container_main_control.appendChild(this._autorotate);
		this._down=document.createElement('div');
		this._down.ggId="down";
		this._down.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._down.ggVisible=true;
		this._down.className='ggskin ggskin_button';
		this._down.ggType='button';
		hs ='position:absolute;';
		hs+='left: 204px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._down.setAttribute('style',hs);
		this._down__img=document.createElement('img');
		this._down__img.className='ggskin ggskin_button';
		this._down__img.setAttribute('src',basePath + 'images/down.png');
		this._down__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._down__img.className='ggskin ggskin_button';
		this._down__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._down__img);
		this._down.appendChild(this._down__img);
		this._down.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._down.style[domTransition]='none';
			} else {
				me._down.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._down.ggParameter.rx=0;me._down.ggParameter.ry=-2;
			me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			me._txt_down.style[domTransition]='none';
			me._txt_down.style.opacity='1';
			me._txt_down.style.visibility=me._txt_down.ggVisible?'inherit':'hidden';
		}
		this._down.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._down.style[domTransition]='none';
			} else {
				me._down.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._down.ggParameter.rx=0;me._down.ggParameter.ry=0;
			me._down.style[domTransform]=parameterToTransform(me._down.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_down.style[domTransition]='none';
			} else {
				me._txt_down.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_down.style.opacity='0';
			me._txt_down.style.visibility='hidden';
			me.elementMouseDown['down']=false;
		}
		this._down.onmousedown=function () {
			me._down__img.src=basePath + 'images/down__a.png';
			me.elementMouseDown['down']=true;
		}
		this._down.onmouseup=function () {
			me._down__img.src=basePath + 'images/down.png';
			me.elementMouseDown['down']=false;
		}
		this._down.ontouchend=function () {
			me.elementMouseDown['down']=false;
		}
		this._txt_down=document.createElement('div');
		this._txt_down__text=document.createElement('div');
		this._txt_down.className='ggskin ggskin_textdiv';
		this._txt_down.ggTextDiv=this._txt_down__text;
		this._txt_down.ggId="txt down";
		this._txt_down.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_down.ggVisible=true;
		this._txt_down.className='ggskin ggskin_text';
		this._txt_down.ggType='text';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 44px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_down.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 44px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_down__text.setAttribute('style',hs);
		this._txt_down__text.innerHTML="\u0412\u043d\u0438\u0437";
		this._txt_down.appendChild(this._txt_down__text);
		this._down.appendChild(this._txt_down);
		this._container_main_control.appendChild(this._down);
		this._up=document.createElement('div');
		this._up.ggId="up";
		this._up.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._up.ggVisible=true;
		this._up.className='ggskin ggskin_button';
		this._up.ggType='button';
		hs ='position:absolute;';
		hs+='left: 171px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._up.setAttribute('style',hs);
		this._up__img=document.createElement('img');
		this._up__img.className='ggskin ggskin_button';
		this._up__img.setAttribute('src',basePath + 'images/up.png');
		this._up__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._up__img.className='ggskin ggskin_button';
		this._up__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._up__img);
		this._up.appendChild(this._up__img);
		this._up.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._up.style[domTransition]='none';
			} else {
				me._up.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._up.ggParameter.rx=0;me._up.ggParameter.ry=-2;
			me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			me._txt_up.style[domTransition]='none';
			me._txt_up.style.opacity='1';
			me._txt_up.style.visibility=me._txt_up.ggVisible?'inherit':'hidden';
		}
		this._up.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._up.style[domTransition]='none';
			} else {
				me._up.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._up.ggParameter.rx=0;me._up.ggParameter.ry=0;
			me._up.style[domTransform]=parameterToTransform(me._up.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_up.style[domTransition]='none';
			} else {
				me._txt_up.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_up.style.opacity='0';
			me._txt_up.style.visibility='hidden';
			me.elementMouseDown['up']=false;
		}
		this._up.onmousedown=function () {
			me._up__img.src=basePath + 'images/up__a.png';
			me.elementMouseDown['up']=true;
		}
		this._up.onmouseup=function () {
			me._up__img.src=basePath + 'images/up.png';
			me.elementMouseDown['up']=false;
		}
		this._up.ontouchend=function () {
			me.elementMouseDown['up']=false;
		}
		this._txt_up=document.createElement('div');
		this._txt_up__text=document.createElement('div');
		this._txt_up.className='ggskin ggskin_textdiv';
		this._txt_up.ggTextDiv=this._txt_up__text;
		this._txt_up.ggId="txt up";
		this._txt_up.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_up.ggVisible=true;
		this._txt_up.className='ggskin ggskin_text';
		this._txt_up.ggType='text';
		this._txt_up.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (46-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 46px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_up.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_up__text.setAttribute('style',hs);
		this._txt_up__text.innerHTML="\u0412\u0432\u0435\u0440\u0445";
		this._txt_up.appendChild(this._txt_up__text);
		this._up.appendChild(this._txt_up);
		this._container_main_control.appendChild(this._up);
		this._right=document.createElement('div');
		this._right.ggId="right";
		this._right.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._right.ggVisible=true;
		this._right.className='ggskin ggskin_button';
		this._right.ggType='button';
		hs ='position:absolute;';
		hs+='left: 138px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._right.setAttribute('style',hs);
		this._right__img=document.createElement('img');
		this._right__img.className='ggskin ggskin_button';
		this._right__img.setAttribute('src',basePath + 'images/right.png');
		this._right__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._right__img.className='ggskin ggskin_button';
		this._right__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._right__img);
		this._right.appendChild(this._right__img);
		this._right.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._right.style[domTransition]='none';
			} else {
				me._right.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._right.ggParameter.rx=0;me._right.ggParameter.ry=-2;
			me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			me._txt_right.style[domTransition]='none';
			me._txt_right.style.opacity='1';
			me._txt_right.style.visibility=me._txt_right.ggVisible?'inherit':'hidden';
		}
		this._right.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._right.style[domTransition]='none';
			} else {
				me._right.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._right.ggParameter.rx=0;me._right.ggParameter.ry=0;
			me._right.style[domTransform]=parameterToTransform(me._right.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_right.style[domTransition]='none';
			} else {
				me._txt_right.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_right.style.opacity='0';
			me._txt_right.style.visibility='hidden';
			me.elementMouseDown['right']=false;
		}
		this._right.onmousedown=function () {
			me._right__img.src=basePath + 'images/right__a.png';
			me.elementMouseDown['right']=true;
		}
		this._right.onmouseup=function () {
			me._right__img.src=basePath + 'images/right.png';
			me.elementMouseDown['right']=false;
		}
		this._right.ontouchend=function () {
			me.elementMouseDown['right']=false;
		}
		this._txt_right=document.createElement('div');
		this._txt_right__text=document.createElement('div');
		this._txt_right.className='ggskin ggskin_textdiv';
		this._txt_right.ggTextDiv=this._txt_right__text;
		this._txt_right.ggId="txt right";
		this._txt_right.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_right.ggVisible=true;
		this._txt_right.className='ggskin ggskin_text';
		this._txt_right.ggType='text';
		this._txt_right.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (52-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 52px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_right.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_right__text.setAttribute('style',hs);
		this._txt_right__text.innerHTML="\u0412\u043f\u0440\u0430\u0432\u043e";
		this._txt_right.appendChild(this._txt_right__text);
		this._right.appendChild(this._txt_right);
		this._container_main_control.appendChild(this._right);
		this._left=document.createElement('div');
		this._left.ggId="left";
		this._left.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._left.ggVisible=true;
		this._left.className='ggskin ggskin_button';
		this._left.ggType='button';
		hs ='position:absolute;';
		hs+='left: 105px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._left.setAttribute('style',hs);
		this._left__img=document.createElement('img');
		this._left__img.className='ggskin ggskin_button';
		this._left__img.setAttribute('src',basePath + 'images/left.png');
		this._left__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._left__img.className='ggskin ggskin_button';
		this._left__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._left__img);
		this._left.appendChild(this._left__img);
		this._left.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._left.style[domTransition]='none';
			} else {
				me._left.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._left.ggParameter.rx=0;me._left.ggParameter.ry=-2;
			me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			me._txt_left.style[domTransition]='none';
			me._txt_left.style.opacity='1';
			me._txt_left.style.visibility=me._txt_left.ggVisible?'inherit':'hidden';
		}
		this._left.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._left.style[domTransition]='none';
			} else {
				me._left.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._left.ggParameter.rx=0;me._left.ggParameter.ry=0;
			me._left.style[domTransform]=parameterToTransform(me._left.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_left.style[domTransition]='none';
			} else {
				me._txt_left.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_left.style.opacity='0';
			me._txt_left.style.visibility='hidden';
			me.elementMouseDown['left']=false;
		}
		this._left.onmousedown=function () {
			me._left__img.src=basePath + 'images/left__a.png';
			me.elementMouseDown['left']=true;
		}
		this._left.onmouseup=function () {
			me._left__img.src=basePath + 'images/left.png';
			me.elementMouseDown['left']=false;
		}
		this._left.ontouchend=function () {
			me.elementMouseDown['left']=false;
		}
		this._txt_left=document.createElement('div');
		this._txt_left__text=document.createElement('div');
		this._txt_left.className='ggskin ggskin_textdiv';
		this._txt_left.ggTextDiv=this._txt_left__text;
		this._txt_left.ggId="txt left";
		this._txt_left.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_left.ggVisible=true;
		this._txt_left.className='ggskin ggskin_text';
		this._txt_left.ggType='text';
		this._txt_left.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (46-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 46px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_left.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_left__text.setAttribute('style',hs);
		this._txt_left__text.innerHTML="\u0412\u043b\u0435\u0432\u043e";
		this._txt_left.appendChild(this._txt_left__text);
		this._left.appendChild(this._txt_left);
		this._container_main_control.appendChild(this._left);
		this._zoom_out=document.createElement('div');
		this._zoom_out.ggId="zoom out";
		this._zoom_out.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._zoom_out.ggVisible=true;
		this._zoom_out.className='ggskin ggskin_button';
		this._zoom_out.ggType='button';
		hs ='position:absolute;';
		hs+='left: 72px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._zoom_out.setAttribute('style',hs);
		this._zoom_out__img=document.createElement('img');
		this._zoom_out__img.className='ggskin ggskin_button';
		this._zoom_out__img.setAttribute('src',basePath + 'images/zoom_out.png');
		this._zoom_out__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoom_out__img.className='ggskin ggskin_button';
		this._zoom_out__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoom_out__img);
		this._zoom_out.appendChild(this._zoom_out__img);
		this._zoom_out.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._zoom_out.style[domTransition]='none';
			} else {
				me._zoom_out.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._zoom_out.ggParameter.rx=0;me._zoom_out.ggParameter.ry=-2;
			me._zoom_out.style[domTransform]=parameterToTransform(me._zoom_out.ggParameter);
			me._txt_zoom_out.style[domTransition]='none';
			me._txt_zoom_out.style.opacity='1';
			me._txt_zoom_out.style.visibility=me._txt_zoom_out.ggVisible?'inherit':'hidden';
		}
		this._zoom_out.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._zoom_out.style[domTransition]='none';
			} else {
				me._zoom_out.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._zoom_out.ggParameter.rx=0;me._zoom_out.ggParameter.ry=0;
			me._zoom_out.style[domTransform]=parameterToTransform(me._zoom_out.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_zoom_out.style[domTransition]='none';
			} else {
				me._txt_zoom_out.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_zoom_out.style.opacity='0';
			me._txt_zoom_out.style.visibility='hidden';
			me.elementMouseDown['zoom_out']=false;
		}
		this._zoom_out.onmousedown=function () {
			me._zoom_out__img.src=basePath + 'images/zoom_out__a.png';
			me.elementMouseDown['zoom_out']=true;
		}
		this._zoom_out.onmouseup=function () {
			me._zoom_out__img.src=basePath + 'images/zoom_out.png';
			me.elementMouseDown['zoom_out']=false;
		}
		this._zoom_out.ontouchend=function () {
			me.elementMouseDown['zoom_out']=false;
		}
		this._txt_zoom_out=document.createElement('div');
		this._txt_zoom_out__text=document.createElement('div');
		this._txt_zoom_out.className='ggskin ggskin_textdiv';
		this._txt_zoom_out.ggTextDiv=this._txt_zoom_out__text;
		this._txt_zoom_out.ggId="txt zoom out";
		this._txt_zoom_out.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_zoom_out.ggVisible=true;
		this._txt_zoom_out.className='ggskin ggskin_text';
		this._txt_zoom_out.ggType='text';
		this._txt_zoom_out.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (73-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 73px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_zoom_out.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_zoom_out__text.setAttribute('style',hs);
		this._txt_zoom_out__text.innerHTML="\u041e\u0442\u0434\u0430\u043b\u0438\u0442\u044c";
		this._txt_zoom_out.appendChild(this._txt_zoom_out__text);
		this._zoom_out.appendChild(this._txt_zoom_out);
		this._container_main_control.appendChild(this._zoom_out);
		this._zoom_in=document.createElement('div');
		this._zoom_in.ggId="zoom in";
		this._zoom_in.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._zoom_in.ggVisible=true;
		this._zoom_in.className='ggskin ggskin_button';
		this._zoom_in.ggType='button';
		hs ='position:absolute;';
		hs+='left: 39px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._zoom_in.setAttribute('style',hs);
		this._zoom_in__img=document.createElement('img');
		this._zoom_in__img.className='ggskin ggskin_button';
		this._zoom_in__img.setAttribute('src',basePath + 'images/zoom_in.png');
		this._zoom_in__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._zoom_in__img.className='ggskin ggskin_button';
		this._zoom_in__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._zoom_in__img);
		this._zoom_in.appendChild(this._zoom_in__img);
		this._zoom_in.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._zoom_in.style[domTransition]='none';
			} else {
				me._zoom_in.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._zoom_in.ggParameter.rx=0;me._zoom_in.ggParameter.ry=-2;
			me._zoom_in.style[domTransform]=parameterToTransform(me._zoom_in.ggParameter);
			me._txt_zoom_in.style[domTransition]='none';
			me._txt_zoom_in.style.opacity='1';
			me._txt_zoom_in.style.visibility=me._txt_zoom_in.ggVisible?'inherit':'hidden';
		}
		this._zoom_in.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._zoom_in.style[domTransition]='none';
			} else {
				me._zoom_in.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._zoom_in.ggParameter.rx=0;me._zoom_in.ggParameter.ry=0;
			me._zoom_in.style[domTransform]=parameterToTransform(me._zoom_in.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_zoom_in.style[domTransition]='none';
			} else {
				me._txt_zoom_in.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_zoom_in.style.opacity='0';
			me._txt_zoom_in.style.visibility='hidden';
			me.elementMouseDown['zoom_in']=false;
		}
		this._zoom_in.onmousedown=function () {
			me._zoom_in__img.src=basePath + 'images/zoom_in__a.png';
			me.elementMouseDown['zoom_in']=true;
		}
		this._zoom_in.onmouseup=function () {
			me._zoom_in__img.src=basePath + 'images/zoom_in.png';
			me.elementMouseDown['zoom_in']=false;
		}
		this._zoom_in.ontouchend=function () {
			me.elementMouseDown['zoom_in']=false;
		}
		this._txt_zoom_in=document.createElement('div');
		this._txt_zoom_in__text=document.createElement('div');
		this._txt_zoom_in.className='ggskin ggskin_textdiv';
		this._txt_zoom_in.ggTextDiv=this._txt_zoom_in__text;
		this._txt_zoom_in.ggId="txt zoom in";
		this._txt_zoom_in.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_zoom_in.ggVisible=true;
		this._txt_zoom_in.className='ggskin ggskin_text';
		this._txt_zoom_in.ggType='text';
		this._txt_zoom_in.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (80-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_zoom_in.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_zoom_in__text.setAttribute('style',hs);
		this._txt_zoom_in__text.innerHTML="\u041f\u0440\u0438\u0431\u043b\u0438\u0437\u0438\u0442\u044c";
		this._txt_zoom_in.appendChild(this._txt_zoom_in__text);
		this._zoom_in.appendChild(this._txt_zoom_in);
		this._container_main_control.appendChild(this._zoom_in);
		this._fullscreen=document.createElement('div');
		this._fullscreen.ggId="fullscreen";
		this._fullscreen.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._fullscreen.ggVisible=true;
		this._fullscreen.className='ggskin ggskin_button';
		this._fullscreen.ggType='button';
		hs ='position:absolute;';
		hs+='left: 6px;';
		hs+='top:  3px;';
		hs+='width: 32px;';
		hs+='height: 32px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._fullscreen.setAttribute('style',hs);
		this._fullscreen__img=document.createElement('img');
		this._fullscreen__img.className='ggskin ggskin_button';
		this._fullscreen__img.setAttribute('src',basePath + 'images/fullscreen.png');
		this._fullscreen__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._fullscreen__img.className='ggskin ggskin_button';
		this._fullscreen__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._fullscreen__img);
		this._fullscreen.appendChild(this._fullscreen__img);
		this._fullscreen.onclick=function () {
			me.player.toggleFullscreen();
		}
		this._fullscreen.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._fullscreen.style[domTransition]='none';
			} else {
				me._fullscreen.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._fullscreen.ggParameter.rx=0;me._fullscreen.ggParameter.ry=-2;
			me._fullscreen.style[domTransform]=parameterToTransform(me._fullscreen.ggParameter);
			me._txt_fullscreen.style[domTransition]='none';
			me._txt_fullscreen.style.opacity='1';
			me._txt_fullscreen.style.visibility=me._txt_fullscreen.ggVisible?'inherit':'hidden';
		}
		this._fullscreen.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._fullscreen.style[domTransition]='none';
			} else {
				me._fullscreen.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._fullscreen.ggParameter.rx=0;me._fullscreen.ggParameter.ry=0;
			me._fullscreen.style[domTransform]=parameterToTransform(me._fullscreen.ggParameter);
			if (me.player.transitionsDisabled) {
				me._txt_fullscreen.style[domTransition]='none';
			} else {
				me._txt_fullscreen.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._txt_fullscreen.style.opacity='0';
			me._txt_fullscreen.style.visibility='hidden';
		}
		this._fullscreen.onmousedown=function () {
			me._fullscreen__img.src=basePath + 'images/fullscreen__a.png';
		}
		this._fullscreen.onmouseup=function () {
			me._fullscreen__img.src=basePath + 'images/fullscreen.png';
		}
		this._txt_fullscreen=document.createElement('div');
		this._txt_fullscreen__text=document.createElement('div');
		this._txt_fullscreen.className='ggskin ggskin_textdiv';
		this._txt_fullscreen.ggTextDiv=this._txt_fullscreen__text;
		this._txt_fullscreen.ggId="txt fullscreen";
		this._txt_fullscreen.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._txt_fullscreen.ggVisible=true;
		this._txt_fullscreen.className='ggskin ggskin_text';
		this._txt_fullscreen.ggType='text';
		this._txt_fullscreen.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (158-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  -24px;';
		hs+='width: 158px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._txt_fullscreen.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._txt_fullscreen__text.setAttribute('style',hs);
		this._txt_fullscreen__text.innerHTML="\u041e\u0442\u043a\u0440\u044b\u0442\u044c \u043d\u0430 \u0432\u0435\u0441\u044c \u044d\u043a\u0440\u0430\u043d";
		this._txt_fullscreen.appendChild(this._txt_fullscreen__text);
		this._fullscreen.appendChild(this._txt_fullscreen);
		this._container_main_control.appendChild(this._fullscreen);
		this.divSkin.appendChild(this._container_main_control);
		this._logo_55=document.createElement('div');
		this._logo_55.ggId="logo_55";
		this._logo_55.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._logo_55.ggVisible=true;
		this._logo_55.className='ggskin ggskin_button';
		this._logo_55.ggType='button';
		this._logo_55.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				w=this.parentNode.offsetWidth;
				this.style.left=(-115 + w) + 'px';
				h=this.parentNode.offsetHeight;
				this.style.top=(-30 + h) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -115px;';
		hs+='top:  -30px;';
		hs+='width: 100px;';
		hs+='height: 19px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.2;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._logo_55.setAttribute('style',hs);
		this._logo_55__img=document.createElement('img');
		this._logo_55__img.className='ggskin ggskin_button';
		this._logo_55__img.setAttribute('src',basePath + 'images/logo_55.png');
		this._logo_55__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._logo_55__img.className='ggskin ggskin_button';
		this._logo_55__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._logo_55__img);
		this._logo_55.appendChild(this._logo_55__img);
		this._logo_55.onclick=function () {
			me.player.openUrl("http:\/\/panorama55.ru","_blank");
		}
		this._logo_55.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._logo_55.style[domTransition]='none';
			} else {
				me._logo_55.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_55.style.opacity='1';
			me._logo_55.style.visibility=me._logo_55.ggVisible?'inherit':'hidden';
		}
		this._logo_55.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._logo_55.style[domTransition]='none';
			} else {
				me._logo_55.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_55.style.opacity='0.2';
			me._logo_55.style.visibility=me._logo_55.ggVisible?'inherit':'hidden';
		}
		this.divSkin.appendChild(this._logo_55);
		this._container_maps=document.createElement('div');
		this._container_maps.ggId="Container maps";
		this._container_maps.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_maps.ggVisible=true;
		this._container_maps.className='ggskin ggskin_container';
		this._container_maps.ggType='container';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 400px;';
		hs+='height: 372px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._container_maps.setAttribute('style',hs);
		this._maps=document.createElement('div');
		this._maps.ggId="maps";
		this._maps.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._maps.ggVisible=true;
		this._maps.className='ggskin ggskin_image';
		this._maps.ggType='image';
		hs ='position:absolute;';
		hs+='left: 4px;';
		hs+='top:  4px;';
		hs+='width: 350px;';
		hs+='height: 338px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.7;';
		hs+='visibility: inherit;';
		this._maps.setAttribute('style',hs);
		this._maps__img=document.createElement('img');
		this._maps__img.className='ggskin ggskin_image';
		this._maps__img.setAttribute('src',basePath + 'images/maps.png');
		this._maps__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._maps__img.className='ggskin ggskin_image';
		this._maps__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._maps__img);
		this._maps.appendChild(this._maps__img);
		this._maps.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._maps.style[domTransition]='none';
			} else {
				me._maps.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps.style.opacity='1';
			me._maps.style.visibility=me._maps.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps_fon.style[domTransition]='none';
			} else {
				me._maps_fon.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps_fon.style.opacity='0.4';
			me._maps_fon.style.visibility=me._maps_fon.ggVisible?'inherit':'hidden';
		}
		this._maps.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._maps.style[domTransition]='none';
			} else {
				me._maps.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps.style.opacity='0.7';
			me._maps.style.visibility=me._maps.ggVisible?'inherit':'hidden';
			if (me.player.transitionsDisabled) {
				me._maps_fon.style[domTransition]='none';
			} else {
				me._maps_fon.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._maps_fon.style.opacity='0';
			me._maps_fon.style.visibility='hidden';
		}
		this._maps_fon=document.createElement('div');
		this._maps_fon.ggId="maps fon";
		this._maps_fon.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._maps_fon.ggVisible=true;
		this._maps_fon.className='ggskin ggskin_image';
		this._maps_fon.ggType='image';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 350px;';
		hs+='height: 338px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._maps_fon.setAttribute('style',hs);
		this._maps_fon__img=document.createElement('img');
		this._maps_fon__img.className='ggskin ggskin_image';
		this._maps_fon__img.setAttribute('src',basePath + 'images/maps_fon.png');
		this._maps_fon__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._maps_fon__img.className='ggskin ggskin_image';
		this._maps_fon__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._maps_fon__img);
		this._maps_fon.appendChild(this._maps_fon__img);
		this._maps.appendChild(this._maps_fon);
		this._marker_node1=document.createElement('div');
		this._marker_node1.ggId="marker_node1";
		this._marker_node1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node1.ggVisible=true;
		this._marker_node1.className='ggskin ggskin_mark';
		this._marker_node1.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 123px;';
		hs+='top:  205px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node1.setAttribute('style',hs);
		this._marker_node1.ggMarkerNodeId='{node3}';
		nodeMarker.push(this._marker_node1);
		this._marker_node1.onclick=function () {
			me.player.openNext('{node3}');
		}
		this._marker_node1.onmouseover=function () {
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility='inherit';
			me._text_1.ggVisible=true;
		}
		this._marker_node1.onmouseout=function () {
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility='hidden';
			me._text_1.ggVisible=false;
		}
		this._text_1=document.createElement('div');
		this._text_1__text=document.createElement('div');
		this._text_1.className='ggskin ggskin_textdiv';
		this._text_1.ggTextDiv=this._text_1__text;
		this._text_1.ggId="Text 1";
		this._text_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_1.ggVisible=false;
		this._text_1.className='ggskin ggskin_text';
		this._text_1.ggType='text';
		this._text_1.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (28-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 73px;';
		hs+='top:  51px;';
		hs+='width: 26px;';
		hs+='height: 23px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_1.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_1__text.setAttribute('style',hs);
		this._text_1__text.innerHTML="<b><\/b>";
		this._text_1.appendChild(this._text_1__text);
		this._marker_node1.appendChild(this._text_1);
		this._maps.appendChild(this._marker_node1);
		this._marker_node4=document.createElement('div');
		this._marker_node4.ggId="marker_node4";
		this._marker_node4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node4.ggVisible=true;
		this._marker_node4.className='ggskin ggskin_mark';
		this._marker_node4.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 133px;';
		hs+='top:  26px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node4.setAttribute('style',hs);
		this._marker_node4.ggMarkerNodeId='{node6}';
		nodeMarker.push(this._marker_node4);
		this._marker_node4.onclick=function () {
			me.player.openNext('{node6}');
		}
		this._marker_node4.onmouseover=function () {
			me._text_4.style[domTransition]='none';
			me._text_4.style.visibility='inherit';
			me._text_4.ggVisible=true;
		}
		this._marker_node4.onmouseout=function () {
			me._text_4.style[domTransition]='none';
			me._text_4.style.visibility='hidden';
			me._text_4.ggVisible=false;
		}
		this._text_4=document.createElement('div');
		this._text_4__text=document.createElement('div');
		this._text_4.className='ggskin ggskin_textdiv';
		this._text_4.ggTextDiv=this._text_4__text;
		this._text_4.ggId="Text 4";
		this._text_4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_4.ggVisible=false;
		this._text_4.className='ggskin ggskin_text';
		this._text_4.ggType='text';
		this._text_4.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 63px;';
		hs+='top:  230px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_4.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_4__text.setAttribute('style',hs);
		this._text_4__text.innerHTML="<b>\u0417\u0430\u043b \u2116 1<\/b>";
		this._text_4.appendChild(this._text_4__text);
		this._marker_node4.appendChild(this._text_4);
		this._maps.appendChild(this._marker_node4);
		this._marker_node5=document.createElement('div');
		this._marker_node5.ggId="marker_node5";
		this._marker_node5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node5.ggVisible=true;
		this._marker_node5.className='ggskin ggskin_mark';
		this._marker_node5.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 38px;';
		hs+='top:  181px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node5.setAttribute('style',hs);
		this._marker_node5.ggMarkerNodeId='{node7}';
		nodeMarker.push(this._marker_node5);
		this._marker_node5.onclick=function () {
			me.player.openNext('{node7}');
		}
		this._marker_node5.onmouseover=function () {
			me._text_5.style[domTransition]='none';
			me._text_5.style.visibility='inherit';
			me._text_5.ggVisible=true;
		}
		this._marker_node5.onmouseout=function () {
			me._text_5.style[domTransition]='none';
			me._text_5.style.visibility='hidden';
			me._text_5.ggVisible=false;
		}
		this._text_5=document.createElement('div');
		this._text_5__text=document.createElement('div');
		this._text_5.className='ggskin ggskin_textdiv';
		this._text_5.ggTextDiv=this._text_5__text;
		this._text_5.ggId="Text 5";
		this._text_5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_5.ggVisible=false;
		this._text_5.className='ggskin ggskin_text';
		this._text_5.ggType='text';
		this._text_5.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 158px;';
		hs+='top:  75px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_5.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_5__text.setAttribute('style',hs);
		this._text_5__text.innerHTML="<b>\u0417\u0430\u043b \u2116 3<\/b>";
		this._text_5.appendChild(this._text_5__text);
		this._marker_node5.appendChild(this._text_5);
		this._maps.appendChild(this._marker_node5);
		this._marker_node8=document.createElement('div');
		this._marker_node8.ggId="marker_node8";
		this._marker_node8.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node8.ggVisible=true;
		this._marker_node8.className='ggskin ggskin_mark';
		this._marker_node8.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 136px;';
		hs+='top:  221px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node8.setAttribute('style',hs);
		this._marker_node8.ggMarkerNodeId='{node1}';
		nodeMarker.push(this._marker_node8);
		this._marker_node8.onclick=function () {
			me.player.openNext('{node1}');
		}
		this._marker_node8.onmouseover=function () {
			me._text_8.style[domTransition]='none';
			me._text_8.style.visibility='inherit';
			me._text_8.ggVisible=true;
		}
		this._marker_node8.onmouseout=function () {
			me._text_8.style[domTransition]='none';
			me._text_8.style.visibility='hidden';
			me._text_8.ggVisible=false;
		}
		this._text_8=document.createElement('div');
		this._text_8__text=document.createElement('div');
		this._text_8.className='ggskin ggskin_textdiv';
		this._text_8.ggTextDiv=this._text_8__text;
		this._text_8.ggId="Text 8";
		this._text_8.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_8.ggVisible=false;
		this._text_8.className='ggskin ggskin_text';
		this._text_8.ggType='text';
		this._text_8.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (74-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 60px;';
		hs+='top:  35px;';
		hs+='width: 70px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_8.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_8__text.setAttribute('style',hs);
		this._text_8__text.innerHTML="<b>\u0413\u0430\u0440\u0434\u0435\u0440\u043e\u0431<\/b>";
		this._text_8.appendChild(this._text_8__text);
		this._marker_node8.appendChild(this._text_8);
		this._maps.appendChild(this._marker_node8);
		this._marker_node9=document.createElement('div');
		this._marker_node9.ggId="marker_node9";
		this._marker_node9.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node9.ggVisible=true;
		this._marker_node9.className='ggskin ggskin_mark';
		this._marker_node9.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 115px;';
		hs+='top:  315px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node9.setAttribute('style',hs);
		this._marker_node9.ggMarkerNodeId='{node2}';
		nodeMarker.push(this._marker_node9);
		this._marker_node9.onclick=function () {
			me.player.openNext('{node2}');
		}
		this._marker_node9.onmouseover=function () {
			me._text_9.style[domTransition]='none';
			me._text_9.style.visibility='inherit';
			me._text_9.ggVisible=true;
		}
		this._marker_node9.onmouseout=function () {
			me._text_9.style[domTransition]='none';
			me._text_9.style.visibility='hidden';
			me._text_9.ggVisible=false;
		}
		this._text_9=document.createElement('div');
		this._text_9__text=document.createElement('div');
		this._text_9.className='ggskin ggskin_textdiv';
		this._text_9.ggTextDiv=this._text_9__text;
		this._text_9.ggId="Text 9";
		this._text_9.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_9.ggVisible=false;
		this._text_9.className='ggskin ggskin_text';
		this._text_9.ggType='text';
		this._text_9.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (74-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 81px;';
		hs+='top:  -59px;';
		hs+='width: 70px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_9.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_9__text.setAttribute('style',hs);
		this._text_9__text.innerHTML="<b>\u0413\u0430\u0440\u0434\u0435\u0440\u043e\u0431<\/b>";
		this._text_9.appendChild(this._text_9__text);
		this._marker_node9.appendChild(this._text_9);
		this._maps.appendChild(this._marker_node9);
		this._marker_node13=document.createElement('div');
		this._marker_node13.ggId="marker_node13";
		this._marker_node13.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node13.ggVisible=true;
		this._marker_node13.className='ggskin ggskin_mark';
		this._marker_node13.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 123px;';
		hs+='top:  169px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node13.setAttribute('style',hs);
		this._marker_node13.ggMarkerNodeId='{node12}';
		nodeMarker.push(this._marker_node13);
		this._marker_node13.onclick=function () {
			me.player.openNext('{node12}');
		}
		this._marker_node13.onmouseover=function () {
			me._text_13.style[domTransition]='none';
			me._text_13.style.visibility='inherit';
			me._text_13.ggVisible=true;
		}
		this._marker_node13.onmouseout=function () {
			me._text_13.style[domTransition]='none';
			me._text_13.style.visibility='hidden';
			me._text_13.ggVisible=false;
		}
		this._text_13=document.createElement('div');
		this._text_13__text=document.createElement('div');
		this._text_13.className='ggskin ggskin_textdiv';
		this._text_13.ggTextDiv=this._text_13__text;
		this._text_13.ggId="Text 13";
		this._text_13.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_13.ggVisible=false;
		this._text_13.className='ggskin ggskin_text';
		this._text_13.ggType='text';
		this._text_13.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 73px;';
		hs+='top:  87px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_13.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_13__text.setAttribute('style',hs);
		this._text_13__text.innerHTML="<b>\u0417\u0430\u043b \u2116 1<\/b>";
		this._text_13.appendChild(this._text_13__text);
		this._marker_node13.appendChild(this._text_13);
		this._maps.appendChild(this._marker_node13);
		this._marker_node15=document.createElement('div');
		this._marker_node15.ggId="marker_node15";
		this._marker_node15.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node15.ggVisible=true;
		this._marker_node15.className='ggskin ggskin_mark';
		this._marker_node15.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 127px;';
		hs+='top:  117px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node15.setAttribute('style',hs);
		this._marker_node15.ggMarkerNodeId='{node16}';
		nodeMarker.push(this._marker_node15);
		this._marker_node15.onclick=function () {
			me.player.openNext('{node16}');
		}
		this._marker_node15.onmouseover=function () {
			me._text_15.style[domTransition]='none';
			me._text_15.style.visibility='inherit';
			me._text_15.ggVisible=true;
		}
		this._marker_node15.onmouseout=function () {
			me._text_15.style[domTransition]='none';
			me._text_15.style.visibility='hidden';
			me._text_15.ggVisible=false;
		}
		this._text_15=document.createElement('div');
		this._text_15__text=document.createElement('div');
		this._text_15.className='ggskin ggskin_textdiv';
		this._text_15.ggTextDiv=this._text_15__text;
		this._text_15.ggId="Text 15";
		this._text_15.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_15.ggVisible=false;
		this._text_15.className='ggskin ggskin_text';
		this._text_15.ggType='text';
		this._text_15.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 69px;';
		hs+='top:  139px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_15.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_15__text.setAttribute('style',hs);
		this._text_15__text.innerHTML="<b>\u0417\u0430\u043b \u2116 1<\/b>";
		this._text_15.appendChild(this._text_15__text);
		this._marker_node15.appendChild(this._text_15);
		this._maps.appendChild(this._marker_node15);
		this._marker_node16=document.createElement('div');
		this._marker_node16.ggId="marker_node16";
		this._marker_node16.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node16.ggVisible=true;
		this._marker_node16.className='ggskin ggskin_mark';
		this._marker_node16.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 300px;';
		hs+='top:  151px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node16.setAttribute('style',hs);
		this._marker_node16.ggMarkerNodeId='{node11}';
		nodeMarker.push(this._marker_node16);
		this._marker_node16.onclick=function () {
			me.player.openNext('{node11}');
		}
		this._marker_node16.onmouseover=function () {
			me._text_16.style[domTransition]='none';
			me._text_16.style.visibility='inherit';
			me._text_16.ggVisible=true;
		}
		this._marker_node16.onmouseout=function () {
			me._text_16.style[domTransition]='none';
			me._text_16.style.visibility='hidden';
			me._text_16.ggVisible=false;
		}
		this._text_16=document.createElement('div');
		this._text_16__text=document.createElement('div');
		this._text_16.className='ggskin ggskin_textdiv';
		this._text_16.ggTextDiv=this._text_16__text;
		this._text_16.ggId="Text 16";
		this._text_16.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_16.ggVisible=false;
		this._text_16.className='ggskin ggskin_text';
		this._text_16.ggType='text';
		this._text_16.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: -104px;';
		hs+='top:  105px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_16.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_16__text.setAttribute('style',hs);
		this._text_16__text.innerHTML="<b>\u0417\u0430\u043b \u2116 2<\/b>";
		this._text_16.appendChild(this._text_16__text);
		this._marker_node16.appendChild(this._text_16);
		this._maps.appendChild(this._marker_node16);
		this._marker_node17=document.createElement('div');
		this._marker_node17.ggId="marker_node17";
		this._marker_node17.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node17.ggVisible=true;
		this._marker_node17.className='ggskin ggskin_mark';
		this._marker_node17.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 41px;';
		hs+='top:  60px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node17.setAttribute('style',hs);
		this._marker_node17.ggMarkerNodeId='{node14}';
		nodeMarker.push(this._marker_node17);
		this._marker_node17.onclick=function () {
			me.player.openNext('{node14}');
		}
		this._marker_node17.onmouseover=function () {
			me._text_17.style[domTransition]='none';
			me._text_17.style.visibility='inherit';
			me._text_17.ggVisible=true;
		}
		this._marker_node17.onmouseout=function () {
			me._text_17.style[domTransition]='none';
			me._text_17.style.visibility='hidden';
			me._text_17.ggVisible=false;
		}
		this._text_17=document.createElement('div');
		this._text_17__text=document.createElement('div');
		this._text_17.className='ggskin ggskin_textdiv';
		this._text_17.ggTextDiv=this._text_17__text;
		this._text_17.ggId="Text 17";
		this._text_17.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_17.ggVisible=false;
		this._text_17.className='ggskin ggskin_text';
		this._text_17.ggType='text';
		this._text_17.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (29-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 155px;';
		hs+='top:  196px;';
		hs+='width: 25px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_17.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_17__text.setAttribute('style',hs);
		this._text_17__text.innerHTML="<b>\u0417\u0430\u043b \u2116 4<\/b>";
		this._text_17.appendChild(this._text_17__text);
		this._marker_node17.appendChild(this._text_17);
		this._maps.appendChild(this._marker_node17);
		this._marker_node22=document.createElement('div');
		this._marker_node22.ggId="marker_node22";
		this._marker_node22.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node22.ggVisible=false;
		this._marker_node22.className='ggskin ggskin_mark';
		this._marker_node22.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 31px;';
		hs+='top:  276px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: hidden;';
		this._marker_node22.setAttribute('style',hs);
		this._marker_node22.ggMarkerNodeId='{node15}';
		nodeMarker.push(this._marker_node22);
		this._marker_node22.onclick=function () {
			me.player.openNext('{node15}');
		}
		this._marker_node22.onmouseover=function () {
			me._text_22.style[domTransition]='none';
			me._text_22.style.visibility='inherit';
			me._text_22.ggVisible=true;
		}
		this._marker_node22.onmouseout=function () {
			me._text_22.style[domTransition]='none';
			me._text_22.style.visibility='hidden';
			me._text_22.ggVisible=false;
		}
		this._marker_node22.ggActivate=function () {
			me.player.stopAutorotate();
		}
		this._text_22=document.createElement('div');
		this._text_22__text=document.createElement('div');
		this._text_22.className='ggskin ggskin_textdiv';
		this._text_22.ggTextDiv=this._text_22__text;
		this._text_22.ggId="Text 22";
		this._text_22.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_22.ggVisible=false;
		this._text_22.className='ggskin ggskin_text';
		this._text_22.ggType='text';
		this._text_22.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (49-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 165px;';
		hs+='top:  -20px;';
		hs+='width: 45px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_22.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_22__text.setAttribute('style',hs);
		this._text_22__text.innerHTML="<b>\u0412\u0445\u043e\u0434<\/b>";
		this._text_22.appendChild(this._text_22__text);
		this._marker_node22.appendChild(this._text_22);
		this._maps.appendChild(this._marker_node22);
		this._marker_node36=document.createElement('div');
		this._marker_node36.ggId="marker_node36";
		this._marker_node36.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node36.ggVisible=false;
		this._marker_node36.className='ggskin ggskin_mark';
		this._marker_node36.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 31px;';
		hs+='top:  276px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: hidden;';
		this._marker_node36.setAttribute('style',hs);
		this._marker_node36.ggMarkerNodeId='{node19}';
		nodeMarker.push(this._marker_node36);
		this._marker_node36.onclick=function () {
			me.player.openNext('{node19}');
		}
		this._marker_node36.onmouseover=function () {
			me._text_36.style[domTransition]='none';
			me._text_36.style.visibility='inherit';
			me._text_36.ggVisible=true;
		}
		this._marker_node36.onmouseout=function () {
			me._text_36.style[domTransition]='none';
			me._text_36.style.visibility='hidden';
			me._text_36.ggVisible=false;
		}
		this._marker_node36.ggActivate=function () {
			me.player.stopAutorotate();
		}
		this._text_36=document.createElement('div');
		this._text_36__text=document.createElement('div');
		this._text_36.className='ggskin ggskin_textdiv';
		this._text_36.ggTextDiv=this._text_36__text;
		this._text_36.ggId="Text 36";
		this._text_36.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_36.ggVisible=false;
		this._text_36.className='ggskin ggskin_text';
		this._text_36.ggType='text';
		this._text_36.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (49-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 165px;';
		hs+='top:  -20px;';
		hs+='width: 45px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_36.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_36__text.setAttribute('style',hs);
		this._text_36__text.innerHTML="<b>\u0412\u0445\u043e\u0434<\/b>";
		this._text_36.appendChild(this._text_36__text);
		this._marker_node36.appendChild(this._text_36);
		this._maps.appendChild(this._marker_node36);
		this._marker_node30=document.createElement('div');
		this._marker_node30.ggId="marker_node30";
		this._marker_node30.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node30.ggVisible=true;
		this._marker_node30.className='ggskin ggskin_mark';
		this._marker_node30.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 196px;';
		hs+='top:  152px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node30.setAttribute('style',hs);
		this._marker_node30.ggMarkerNodeId='{node17}';
		nodeMarker.push(this._marker_node30);
		this._marker_node30.onclick=function () {
			me.player.openNext('{node17}');
		}
		this._marker_node30.onmouseover=function () {
			me._text_30.style[domTransition]='none';
			me._text_30.style.visibility='inherit';
			me._text_30.ggVisible=true;
		}
		this._marker_node30.onmouseout=function () {
			me._text_30.style[domTransition]='none';
			me._text_30.style.visibility='hidden';
			me._text_30.ggVisible=false;
		}
		this._text_30=document.createElement('div');
		this._text_30__text=document.createElement('div');
		this._text_30.className='ggskin ggskin_textdiv';
		this._text_30.ggTextDiv=this._text_30__text;
		this._text_30.ggId="Text 30";
		this._text_30.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_30.ggVisible=false;
		this._text_30.className='ggskin ggskin_text';
		this._text_30.ggType='text';
		this._text_30.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (49-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  104px;';
		hs+='width: 45px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_30.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_30__text.setAttribute('style',hs);
		this._text_30__text.innerHTML="<b>\u0417\u0430\u043b \u2116 1<\/b>";
		this._text_30.appendChild(this._text_30__text);
		this._marker_node30.appendChild(this._text_30);
		this._maps.appendChild(this._marker_node30);
		this._marker_node31=document.createElement('div');
		this._marker_node31.ggId="marker_node31";
		this._marker_node31.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_node31.ggVisible=true;
		this._marker_node31.className='ggskin ggskin_mark';
		this._marker_node31.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 176px;';
		hs+='top:  75px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._marker_node31.setAttribute('style',hs);
		this._marker_node31.ggMarkerNodeId='{node18}';
		nodeMarker.push(this._marker_node31);
		this._marker_node31.onclick=function () {
			me.player.openNext('{node18}');
		}
		this._marker_node31.onmouseover=function () {
			me._text_31.style[domTransition]='none';
			me._text_31.style.visibility='inherit';
			me._text_31.ggVisible=true;
		}
		this._marker_node31.onmouseout=function () {
			me._text_31.style[domTransition]='none';
			me._text_31.style.visibility='hidden';
			me._text_31.ggVisible=false;
		}
		this._text_31=document.createElement('div');
		this._text_31__text=document.createElement('div');
		this._text_31.className='ggskin ggskin_textdiv';
		this._text_31.ggTextDiv=this._text_31__text;
		this._text_31.ggId="Text 31";
		this._text_31.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_31.ggVisible=false;
		this._text_31.className='ggskin ggskin_text';
		this._text_31.ggType='text';
		this._text_31.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			this.style.width=this.ggTextDiv.offsetWidth + 'px';
			this.style.height=this.ggTextDiv.offsetHeight + 'px';
			this.ggTextDiv.style.left=(0 + (49-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		hs ='position:absolute;';
		hs+='left: 20px;';
		hs+='top:  181px;';
		hs+='width: 45px;';
		hs+='height: 22px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0.8;';
		hs+='visibility: hidden;';
		this._text_31.setAttribute('style',hs);
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #cec57c;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 6px;';
		hs+=cssPrefix + 'border-radius: 6px;';
		hs+='color: #ffffff;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._text_31__text.setAttribute('style',hs);
		this._text_31__text.innerHTML="<b>\u0417\u0430\u043b \u2116 1<\/b>";
		this._text_31.appendChild(this._text_31__text);
		this._marker_node31.appendChild(this._text_31);
		this._maps.appendChild(this._marker_node31);
		this._container_maps.appendChild(this._maps);
		this._container_marker=document.createElement('div');
		this._container_marker.ggId="Container_marker";
		this._container_marker.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._container_marker.ggVisible=true;
		this._container_marker.className='ggskin ggskin_container';
		this._container_marker.ggType='container';
		hs ='position:absolute;';
		hs+='left: 4px;';
		hs+='top:  5px;';
		hs+='width: 175px;';
		hs+='height: 158px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='opacity: 0;';
		hs+='visibility: hidden;';
		this._container_marker.setAttribute('style',hs);
		this._markertemplate=document.createElement('div');
		this._markertemplate.ggId="markertemplate";
		this._markertemplate.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._markertemplate.ggVisible=true;
		this._markertemplate.className='ggskin ggskin_mark';
		this._markertemplate.ggType='mark';
		hs ='position:absolute;';
		hs+='left: 24px;';
		hs+='top:  18px;';
		hs+='width: 5px;';
		hs+='height: 5px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		this._markertemplate.setAttribute('style',hs);
		this._markertemplate.ggMarkerNodeId='';
		nodeMarker.push(this._markertemplate);
		this._marker_normal=document.createElement('div');
		this._marker_normal.ggId="marker_normal";
		this._marker_normal.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_normal.ggVisible=true;
		this._marker_normal.className='ggskin ggskin_button';
		this._marker_normal.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 20px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._marker_normal.setAttribute('style',hs);
		this._marker_normal__img=document.createElement('img');
		this._marker_normal__img.className='ggskin ggskin_button';
		this._marker_normal__img.setAttribute('src',basePath + 'images/marker_normal.png');
		this._marker_normal__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._marker_normal__img.className='ggskin ggskin_button';
		this._marker_normal__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._marker_normal__img);
		this._marker_normal.appendChild(this._marker_normal__img);
		this._markertemplate.appendChild(this._marker_normal);
		this._marker_aktiv=document.createElement('div');
		this._marker_aktiv.ggId="marker_aktiv";
		this._marker_aktiv.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._marker_aktiv.ggVisible=true;
		this._marker_aktiv.className='ggskin ggskin_button';
		this._marker_aktiv.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 20px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._marker_aktiv.setAttribute('style',hs);
		this._marker_aktiv__img=document.createElement('img');
		this._marker_aktiv__img.className='ggskin ggskin_button';
		this._marker_aktiv__img.setAttribute('src',basePath + 'images/marker_aktiv.png');
		this._marker_aktiv__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._marker_aktiv__img.className='ggskin ggskin_button';
		this._marker_aktiv__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._marker_aktiv__img);
		this._marker_aktiv.appendChild(this._marker_aktiv__img);
		this._maps_t=document.createElement('div');
		this._maps_t.ggId="maps_t";
		this._maps_t.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._maps_t.ggVisible=true;
		this._maps_t.className='ggskin ggskin_button';
		this._maps_t.ggType='button';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 20px;';
		hs+='height: 20px;';
		hs+=cssPrefix + 'transform-origin: 50% 50%;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._maps_t.setAttribute('style',hs);
		this._maps_t__img=document.createElement('img');
		this._maps_t__img.className='ggskin ggskin_button';
		this._maps_t__img.setAttribute('src',basePath + 'images/maps_t.png');
		this._maps_t__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._maps_t__img.className='ggskin ggskin_button';
		this._maps_t__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._maps_t__img);
		this._maps_t.appendChild(this._maps_t__img);
		this._marker_aktiv.appendChild(this._maps_t);
		this._markertemplate.appendChild(this._marker_aktiv);
		this._container_marker.appendChild(this._markertemplate);
		this._container_maps.appendChild(this._container_marker);
		this.divSkin.appendChild(this._container_maps);
		this._logo_py6kin=document.createElement('div');
		this._logo_py6kin.ggId="Logo_py6kin";
		this._logo_py6kin.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._logo_py6kin.ggVisible=true;
		this._logo_py6kin.className='ggskin ggskin_button';
		this._logo_py6kin.ggType='button';
		this._logo_py6kin.ggUpdatePosition=function() {
			this.style[domTransition]='none';
			if (this.parentNode) {
				w=this.parentNode.offsetWidth;
				this.style.left=(-128 + w) + 'px';
			}
		}
		hs ='position:absolute;';
		hs+='left: -128px;';
		hs+='top:  3px;';
		hs+='width: 124px;';
		hs+='height: 206px;';
		hs+=cssPrefix + 'transform-origin: 100% 0%;';
		hs+='opacity: 0.6;';
		hs+='visibility: inherit;';
		hs+='cursor: pointer;';
		this._logo_py6kin.setAttribute('style',hs);
		this._logo_py6kin__img=document.createElement('img');
		this._logo_py6kin__img.className='ggskin ggskin_button';
		this._logo_py6kin__img.setAttribute('src',basePath + 'images/logo_py6kin.png');
		this._logo_py6kin__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
		this._logo_py6kin__img.className='ggskin ggskin_button';
		this._logo_py6kin__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this._logo_py6kin__img);
		this._logo_py6kin.appendChild(this._logo_py6kin__img);
		this._logo_py6kin.onclick=function () {
			me.player.openUrl("https:\/\/vk.com\/kochegarka_omsk","_blank");
		}
		this._logo_py6kin.onmouseover=function () {
			if (me.player.transitionsDisabled) {
				me._logo_py6kin.style[domTransition]='none';
			} else {
				me._logo_py6kin.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_py6kin.style.opacity='1';
			me._logo_py6kin.style.visibility=me._logo_py6kin.ggVisible?'inherit':'hidden';
		}
		this._logo_py6kin.onmouseout=function () {
			if (me.player.transitionsDisabled) {
				me._logo_py6kin.style[domTransition]='none';
			} else {
				me._logo_py6kin.style[domTransition]='all 500ms ease-out 0ms';
			}
			me._logo_py6kin.style.opacity='0.5';
			me._logo_py6kin.style.visibility=me._logo_py6kin.ggVisible?'inherit':'hidden';
		}
		this.divSkin.appendChild(this._logo_py6kin);
		this._marker_node1__normal=this._marker_normal.cloneNode(true);
		this._marker_node1__normal.style.visibility='inherit';
		this._marker_node1__normal.style.left=0;
		this._marker_node1__normal.style.top=0;
		this._marker_node1.ggMarkerNormal=this._marker_node1__normal;
		this._marker_node1__active=this._marker_aktiv.cloneNode(true);
		this._marker_node1__active.style.visibility='hidden';
		this._marker_node1__active.style.left=0;
		this._marker_node1__active.style.top=0;
		this._marker_node1.ggMarkerActive=this._marker_node1__active;
		if (this._marker_node1.firstChild) {
			this._marker_node1.insertBefore(this._marker_node1__active,this._marker_node1.firstChild);
		} else {
			this._marker_node1.appendChild(this._marker_node1__active);
		}
		if (this._marker_node1.firstChild) {
			this._marker_node1.insertBefore(this._marker_node1__normal,this._marker_node1.firstChild);
		} else {
			this._marker_node1.appendChild(this._marker_node1__normal);
		}
		this._marker_node4__normal=this._marker_normal.cloneNode(true);
		this._marker_node4__normal.style.visibility='inherit';
		this._marker_node4__normal.style.left=0;
		this._marker_node4__normal.style.top=0;
		this._marker_node4.ggMarkerNormal=this._marker_node4__normal;
		this._marker_node4__active=this._marker_aktiv.cloneNode(true);
		this._marker_node4__active.style.visibility='hidden';
		this._marker_node4__active.style.left=0;
		this._marker_node4__active.style.top=0;
		this._marker_node4.ggMarkerActive=this._marker_node4__active;
		if (this._marker_node4.firstChild) {
			this._marker_node4.insertBefore(this._marker_node4__active,this._marker_node4.firstChild);
		} else {
			this._marker_node4.appendChild(this._marker_node4__active);
		}
		if (this._marker_node4.firstChild) {
			this._marker_node4.insertBefore(this._marker_node4__normal,this._marker_node4.firstChild);
		} else {
			this._marker_node4.appendChild(this._marker_node4__normal);
		}
		this._marker_node5__normal=this._marker_normal.cloneNode(true);
		this._marker_node5__normal.style.visibility='inherit';
		this._marker_node5__normal.style.left=0;
		this._marker_node5__normal.style.top=0;
		this._marker_node5.ggMarkerNormal=this._marker_node5__normal;
		this._marker_node5__active=this._marker_aktiv.cloneNode(true);
		this._marker_node5__active.style.visibility='hidden';
		this._marker_node5__active.style.left=0;
		this._marker_node5__active.style.top=0;
		this._marker_node5.ggMarkerActive=this._marker_node5__active;
		if (this._marker_node5.firstChild) {
			this._marker_node5.insertBefore(this._marker_node5__active,this._marker_node5.firstChild);
		} else {
			this._marker_node5.appendChild(this._marker_node5__active);
		}
		if (this._marker_node5.firstChild) {
			this._marker_node5.insertBefore(this._marker_node5__normal,this._marker_node5.firstChild);
		} else {
			this._marker_node5.appendChild(this._marker_node5__normal);
		}
		this._marker_node8__normal=this._marker_normal.cloneNode(true);
		this._marker_node8__normal.style.visibility='inherit';
		this._marker_node8__normal.style.left=0;
		this._marker_node8__normal.style.top=0;
		this._marker_node8.ggMarkerNormal=this._marker_node8__normal;
		this._marker_node8__active=this._marker_aktiv.cloneNode(true);
		this._marker_node8__active.style.visibility='hidden';
		this._marker_node8__active.style.left=0;
		this._marker_node8__active.style.top=0;
		this._marker_node8.ggMarkerActive=this._marker_node8__active;
		if (this._marker_node8.firstChild) {
			this._marker_node8.insertBefore(this._marker_node8__active,this._marker_node8.firstChild);
		} else {
			this._marker_node8.appendChild(this._marker_node8__active);
		}
		if (this._marker_node8.firstChild) {
			this._marker_node8.insertBefore(this._marker_node8__normal,this._marker_node8.firstChild);
		} else {
			this._marker_node8.appendChild(this._marker_node8__normal);
		}
		this._marker_node9__normal=this._marker_normal.cloneNode(true);
		this._marker_node9__normal.style.visibility='inherit';
		this._marker_node9__normal.style.left=0;
		this._marker_node9__normal.style.top=0;
		this._marker_node9.ggMarkerNormal=this._marker_node9__normal;
		this._marker_node9__active=this._marker_aktiv.cloneNode(true);
		this._marker_node9__active.style.visibility='hidden';
		this._marker_node9__active.style.left=0;
		this._marker_node9__active.style.top=0;
		this._marker_node9.ggMarkerActive=this._marker_node9__active;
		if (this._marker_node9.firstChild) {
			this._marker_node9.insertBefore(this._marker_node9__active,this._marker_node9.firstChild);
		} else {
			this._marker_node9.appendChild(this._marker_node9__active);
		}
		if (this._marker_node9.firstChild) {
			this._marker_node9.insertBefore(this._marker_node9__normal,this._marker_node9.firstChild);
		} else {
			this._marker_node9.appendChild(this._marker_node9__normal);
		}
		this._marker_node13__normal=this._marker_normal.cloneNode(true);
		this._marker_node13__normal.style.visibility='inherit';
		this._marker_node13__normal.style.left=0;
		this._marker_node13__normal.style.top=0;
		this._marker_node13.ggMarkerNormal=this._marker_node13__normal;
		this._marker_node13__active=this._marker_aktiv.cloneNode(true);
		this._marker_node13__active.style.visibility='hidden';
		this._marker_node13__active.style.left=0;
		this._marker_node13__active.style.top=0;
		this._marker_node13.ggMarkerActive=this._marker_node13__active;
		if (this._marker_node13.firstChild) {
			this._marker_node13.insertBefore(this._marker_node13__active,this._marker_node13.firstChild);
		} else {
			this._marker_node13.appendChild(this._marker_node13__active);
		}
		if (this._marker_node13.firstChild) {
			this._marker_node13.insertBefore(this._marker_node13__normal,this._marker_node13.firstChild);
		} else {
			this._marker_node13.appendChild(this._marker_node13__normal);
		}
		this._marker_node15__normal=this._marker_normal.cloneNode(true);
		this._marker_node15__normal.style.visibility='inherit';
		this._marker_node15__normal.style.left=0;
		this._marker_node15__normal.style.top=0;
		this._marker_node15.ggMarkerNormal=this._marker_node15__normal;
		this._marker_node15__active=this._marker_aktiv.cloneNode(true);
		this._marker_node15__active.style.visibility='hidden';
		this._marker_node15__active.style.left=0;
		this._marker_node15__active.style.top=0;
		this._marker_node15.ggMarkerActive=this._marker_node15__active;
		if (this._marker_node15.firstChild) {
			this._marker_node15.insertBefore(this._marker_node15__active,this._marker_node15.firstChild);
		} else {
			this._marker_node15.appendChild(this._marker_node15__active);
		}
		if (this._marker_node15.firstChild) {
			this._marker_node15.insertBefore(this._marker_node15__normal,this._marker_node15.firstChild);
		} else {
			this._marker_node15.appendChild(this._marker_node15__normal);
		}
		this._marker_node16__normal=this._marker_normal.cloneNode(true);
		this._marker_node16__normal.style.visibility='inherit';
		this._marker_node16__normal.style.left=0;
		this._marker_node16__normal.style.top=0;
		this._marker_node16.ggMarkerNormal=this._marker_node16__normal;
		this._marker_node16__active=this._marker_aktiv.cloneNode(true);
		this._marker_node16__active.style.visibility='hidden';
		this._marker_node16__active.style.left=0;
		this._marker_node16__active.style.top=0;
		this._marker_node16.ggMarkerActive=this._marker_node16__active;
		if (this._marker_node16.firstChild) {
			this._marker_node16.insertBefore(this._marker_node16__active,this._marker_node16.firstChild);
		} else {
			this._marker_node16.appendChild(this._marker_node16__active);
		}
		if (this._marker_node16.firstChild) {
			this._marker_node16.insertBefore(this._marker_node16__normal,this._marker_node16.firstChild);
		} else {
			this._marker_node16.appendChild(this._marker_node16__normal);
		}
		this._marker_node17__normal=this._marker_normal.cloneNode(true);
		this._marker_node17__normal.style.visibility='inherit';
		this._marker_node17__normal.style.left=0;
		this._marker_node17__normal.style.top=0;
		this._marker_node17.ggMarkerNormal=this._marker_node17__normal;
		this._marker_node17__active=this._marker_aktiv.cloneNode(true);
		this._marker_node17__active.style.visibility='hidden';
		this._marker_node17__active.style.left=0;
		this._marker_node17__active.style.top=0;
		this._marker_node17.ggMarkerActive=this._marker_node17__active;
		if (this._marker_node17.firstChild) {
			this._marker_node17.insertBefore(this._marker_node17__active,this._marker_node17.firstChild);
		} else {
			this._marker_node17.appendChild(this._marker_node17__active);
		}
		if (this._marker_node17.firstChild) {
			this._marker_node17.insertBefore(this._marker_node17__normal,this._marker_node17.firstChild);
		} else {
			this._marker_node17.appendChild(this._marker_node17__normal);
		}
		this._marker_node22__normal=this._marker_normal.cloneNode(true);
		this._marker_node22__normal.style.visibility='inherit';
		this._marker_node22__normal.style.left=0;
		this._marker_node22__normal.style.top=0;
		this._marker_node22.ggMarkerNormal=this._marker_node22__normal;
		this._marker_node22__active=this._marker_aktiv.cloneNode(true);
		this._marker_node22__active.style.visibility='hidden';
		this._marker_node22__active.style.left=0;
		this._marker_node22__active.style.top=0;
		this._marker_node22.ggMarkerActive=this._marker_node22__active;
		if (this._marker_node22.firstChild) {
			this._marker_node22.insertBefore(this._marker_node22__active,this._marker_node22.firstChild);
		} else {
			this._marker_node22.appendChild(this._marker_node22__active);
		}
		if (this._marker_node22.firstChild) {
			this._marker_node22.insertBefore(this._marker_node22__normal,this._marker_node22.firstChild);
		} else {
			this._marker_node22.appendChild(this._marker_node22__normal);
		}
		this._marker_node36__normal=this._marker_normal.cloneNode(true);
		this._marker_node36__normal.style.visibility='inherit';
		this._marker_node36__normal.style.left=0;
		this._marker_node36__normal.style.top=0;
		this._marker_node36.ggMarkerNormal=this._marker_node36__normal;
		this._marker_node36__active=this._marker_aktiv.cloneNode(true);
		this._marker_node36__active.style.visibility='hidden';
		this._marker_node36__active.style.left=0;
		this._marker_node36__active.style.top=0;
		this._marker_node36.ggMarkerActive=this._marker_node36__active;
		if (this._marker_node36.firstChild) {
			this._marker_node36.insertBefore(this._marker_node36__active,this._marker_node36.firstChild);
		} else {
			this._marker_node36.appendChild(this._marker_node36__active);
		}
		if (this._marker_node36.firstChild) {
			this._marker_node36.insertBefore(this._marker_node36__normal,this._marker_node36.firstChild);
		} else {
			this._marker_node36.appendChild(this._marker_node36__normal);
		}
		this._marker_node30__normal=this._marker_normal.cloneNode(true);
		this._marker_node30__normal.style.visibility='inherit';
		this._marker_node30__normal.style.left=0;
		this._marker_node30__normal.style.top=0;
		this._marker_node30.ggMarkerNormal=this._marker_node30__normal;
		this._marker_node30__active=this._marker_aktiv.cloneNode(true);
		this._marker_node30__active.style.visibility='hidden';
		this._marker_node30__active.style.left=0;
		this._marker_node30__active.style.top=0;
		this._marker_node30.ggMarkerActive=this._marker_node30__active;
		if (this._marker_node30.firstChild) {
			this._marker_node30.insertBefore(this._marker_node30__active,this._marker_node30.firstChild);
		} else {
			this._marker_node30.appendChild(this._marker_node30__active);
		}
		if (this._marker_node30.firstChild) {
			this._marker_node30.insertBefore(this._marker_node30__normal,this._marker_node30.firstChild);
		} else {
			this._marker_node30.appendChild(this._marker_node30__normal);
		}
		this._marker_node31__normal=this._marker_normal.cloneNode(true);
		this._marker_node31__normal.style.visibility='inherit';
		this._marker_node31__normal.style.left=0;
		this._marker_node31__normal.style.top=0;
		this._marker_node31.ggMarkerNormal=this._marker_node31__normal;
		this._marker_node31__active=this._marker_aktiv.cloneNode(true);
		this._marker_node31__active.style.visibility='hidden';
		this._marker_node31__active.style.left=0;
		this._marker_node31__active.style.top=0;
		this._marker_node31.ggMarkerActive=this._marker_node31__active;
		if (this._marker_node31.firstChild) {
			this._marker_node31.insertBefore(this._marker_node31__active,this._marker_node31.firstChild);
		} else {
			this._marker_node31.appendChild(this._marker_node31__active);
		}
		if (this._marker_node31.firstChild) {
			this._marker_node31.insertBefore(this._marker_node31__normal,this._marker_node31.firstChild);
		} else {
			this._marker_node31.appendChild(this._marker_node31__normal);
		}
		this._markertemplate__normal=this._marker_normal.cloneNode(true);
		this._markertemplate__normal.style.visibility='inherit';
		this._markertemplate__normal.style.left=0;
		this._markertemplate__normal.style.top=0;
		this._markertemplate.ggMarkerNormal=this._markertemplate__normal;
		this._markertemplate__active=this._marker_aktiv.cloneNode(true);
		this._markertemplate__active.style.visibility='hidden';
		this._markertemplate__active.style.left=0;
		this._markertemplate__active.style.top=0;
		this._markertemplate.ggMarkerActive=this._markertemplate__active;
		if (this._markertemplate.firstChild) {
			this._markertemplate.insertBefore(this._markertemplate__active,this._markertemplate.firstChild);
		} else {
			this._markertemplate.appendChild(this._markertemplate__active);
		}
		if (this._markertemplate.firstChild) {
			this._markertemplate.insertBefore(this._markertemplate__normal,this._markertemplate.firstChild);
		} else {
			this._markertemplate.appendChild(this._markertemplate__normal);
		}
		this.preloadImages();
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.changeActiveNode=function(id) {
		var newMarker=new Array();
		var i,j;
		var tags=me.player.userdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId==id) && (id!='')) match=true;
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
			}
		}
		activeNodeMarker=newMarker;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		if (me.elementMouseDown['down']) {
			me.player.changeTiltLog(-1,true);
		}
		if (me.elementMouseDown['up']) {
			me.player.changeTiltLog(1,true);
		}
		if (me.elementMouseDown['right']) {
			me.player.changePanLog(-1,true);
		}
		if (me.elementMouseDown['left']) {
			me.player.changePanLog(1,true);
		}
		if (me.elementMouseDown['zoom_out']) {
			me.player.changeFovLog(1,true);
		}
		if (me.elementMouseDown['zoom_in']) {
			me.player.changeFovLog(-1,true);
		}
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		this.elementMouseDown=new Array();
		this.elementMouseOver=new Array();
		this.__div=document.createElement('div');
		this.__div.setAttribute('style','position:absolute; left:0px;top:0px;visibility: inherit;');
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		{
			this.__div=document.createElement('div');
			this.__div.ggId="Hot_spot";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot';
			this.__div.ggType='hotspot';
			hs ='position:absolute;';
			hs+='left: 346px;';
			hs+='top:  385px;';
			hs+='width: 5px;';
			hs+='height: 5px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			this.__div.setAttribute('style',hs);
			this.__div.onclick=function () {
				me.player.openNext(me.hotspot.url,"");
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function () {
				me.player.hotspot=me.hotspot;
				me.player.stopAutorotate();
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function () {
				me.player.hotspot=me.player.emptyHotspot;
				me.player.startAutorotate("");
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this._hot_spot_iso=document.createElement('div');
			this._hot_spot_iso.ggId="Hot_spot_iso";
			this._hot_spot_iso.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hot_spot_iso.ggVisible=true;
			this._hot_spot_iso.className='ggskin ggskin_button';
			this._hot_spot_iso.ggType='button';
			hs ='position:absolute;';
			hs+='left: -20px;';
			hs+='top:  -86px;';
			hs+='width: 44px;';
			hs+='height: 93px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: inherit;';
			hs+='cursor: pointer;';
			this._hot_spot_iso.setAttribute('style',hs);
			this._hot_spot_iso__img=document.createElement('img');
			this._hot_spot_iso__img.className='ggskin ggskin_button';
			this._hot_spot_iso__img.setAttribute('src',basePath + 'images/hot_spot_iso.png');
			this._hot_spot_iso__img.setAttribute('style','position: absolute;top: 0px;left: 0px;-webkit-user-drag:none;');
			this._hot_spot_iso__img.className='ggskin ggskin_button';
			this._hot_spot_iso__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._hot_spot_iso__img);
			this._hot_spot_iso.appendChild(this._hot_spot_iso__img);
			this._hot_spot_iso.onmouseover=function () {
				me._hot_spot_iso__img.src=basePath + 'images/hot_spot_iso__o.png';
			}
			this._hot_spot_iso.onmouseout=function () {
				me._hot_spot_iso__img.src=basePath + 'images/hot_spot_iso.png';
			}
			this._hot_spot_txt=document.createElement('div');
			this._hot_spot_txt__text=document.createElement('div');
			this._hot_spot_txt.className='ggskin ggskin_textdiv';
			this._hot_spot_txt.ggTextDiv=this._hot_spot_txt__text;
			this._hot_spot_txt.ggId="Hot_spot_txt";
			this._hot_spot_txt.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hot_spot_txt.ggVisible=false;
			this._hot_spot_txt.className='ggskin ggskin_text';
			this._hot_spot_txt.ggType='text';
			this._hot_spot_txt.ggUpdatePosition=function() {
				this.style[domTransition]='none';
				this.style.width=this.ggTextDiv.offsetWidth + 'px';
				this.style.height=this.ggTextDiv.offsetHeight + 'px';
				this.ggTextDiv.style.left=(0 + (95-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			hs ='position:absolute;';
			hs+='left: -28px;';
			hs+='top:  99px;';
			hs+='width: 95px;';
			hs+='height: 22px;';
			hs+=cssPrefix + 'transform-origin: 50% 50%;';
			hs+='visibility: hidden;';
			this._hot_spot_txt.setAttribute('style',hs);
			hs ='position:absolute;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='border: 0px solid #000000;';
			hs+='color: #ffffff;';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 0px 1px 0px 1px;';
			hs+='overflow: hidden;';
			this._hot_spot_txt__text.setAttribute('style',hs);
			this._hot_spot_txt__text.innerHTML=me.hotspot.title;
			this._hot_spot_txt.appendChild(this._hot_spot_txt__text);
			this._hot_spot_iso.appendChild(this._hot_spot_txt);
			this.__div.appendChild(this._hot_spot_iso);
			if (me.player.transitionsDisabled) {
				me.skin.__div.style[domTransition]='none';
			} else {
				me.skin.__div.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me.skin.__div.ggParameter.a="10000000";
			me.skin.__div.style[domTransform]=parameterToTransform(me.skin.__div.ggParameter);
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	this.addSkin();
};