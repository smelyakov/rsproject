$(function () {

$('.slider').slick({
    accessibility: false,
    centerMode: true,
    lazyLoad: 'ondemand',
    initialSlide: 0,
    focusOnSelect: true,
    variableWidth: true,
    arrows: true,
    prevArrow: '<span class="slick-prev"></span>',
    nextArrow: '<span class="slick-next"></span>',
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
    ]
});

$('.project-page__menu').on('click', '.menu__link', function(e) {
    var self = $(this);
    
    e.preventDefault();
    scrollToSection(self.attr('href'));
    $('.page').removeClass('page_no-scroll');
    $('.mobile-nav').removeClass('mobile-nav_visible');
});

var hash = window.location.hash;

if (hash !== '') {
    setTimeout(function(){
        scrollToSection(hash);
    }, 0);
}

function scrollToSection(id) {
    $('html, body').animate({
        scrollTop: $(id).offset().top
    }, 500);
}

$('.cut').on('click', '.cut__toggle', function (e) {
    $(e.delegateTarget).toggleClass('cut_open');
});

$('.nav-toggle').on('click', function () {
    $('.page').toggleClass('page_no-scroll');
    $('.mobile-nav').toggleClass('mobile-nav_visible');
});

var menu = $('.project-page__nav'),
    jumbo = $('.project-page__intro');

if (menu.length && jumbo.length) {
    $('.project-page__nav-toggle').on('click', function () {
        menu.toggleClass('project-page__nav_offscreen')
            .data('manual', true);
    });

    $(window).on('scroll', function (e) {
        if (!menu.data('manual')) {
            watchMenu($(this).scrollTop());
        }
    });
}


function watchMenu(y) {
    var top = jumbo.offset().top + jumbo.height();

    menu.toggleClass('project-page__nav_offscreen', y >= top);
}
 
});
