#!/usr/bin/env bash

sudo apt-get update --fix-missing

function package() {
    for PACKAGE in $*
    do
        if [[ -z `dpkg -s $PACKAGE 2> /dev/null | grep 'Status:.\+installed'` ]]
        then
            sudo apt-get install -y $PACKAGE
        fi
    done
}

# Update a PHP setting value in all instances of 'php.ini'.
php-settings-update() {
  log-operation "$FUNCNAME" "$@"
  local args
  local settings_name
  local php_ini
  local php_extra
  args=( "$@" )
  PREVIOUS_IFS="$IFS"
  IFS='='
  args="${args[*]}"
  IFS="$PREVIOUS_IFS"
  settings_name="$( echo "$args" | system-escape )"
  for php_ini in $( $SUDO find /etc -type f -iname 'php*.ini' ); do
    php_extra="$( dirname "$php_ini" )/conf.d"
    $SUDO mkdir -p "$php_extra"
    echo "$args" | $SUDO tee "$php_extra/0-$settings_name.ini" >/dev/null
  done
}

# Log an operation
log-operation() {
  local function_name
  local function_values
  local arg
  function_name="$1"
  shift
  for arg in "$@"; do
    function_values="$function_values ""'$( echo "$arg" | sed -e 's#\s\+# #g' )'"
  done
  [ -z "$QUIET" ] && echo -e "${DEBUG_BOLD}$function_name${DEBUG_NORMAL}(""$( echo "$function_values" | sed -e 's#^ ##' -e "s#\s\+''\$##g" )"")...${RESET}"
}

# Escape and normalize a string so it can be used safely in file names, etc.
system-escape() {
  local glue
  glue=${1:--}
  while read arg; do
    echo "${arg,,}" | sed -e 's#[^[:alnum:]]\+#'"$glue"'#g' -e 's#^'"$glue"'\+\|'"$glue"'\+$##g'
  done
}

# Create a database if one doesn't already exist.
mysql-database-create() {
  log-operation "$FUNCNAME" "$@"
  mysql -u root -ppassword -e "CREATE DATABASE IF NOT EXISTS \`$1\` CHARACTER SET ${2:-utf8} COLLATE '${3:-utf8_general_ci}'"
}

if [[ -z `cat /etc/default/locale | grep "UTF-8"` ]]
then
    > /etc/default/locale
    echo 'LANGUAGE="en_US.UTF-8"' >> /etc/default/locale
    echo 'LANG="en_US.UTF-8"' >> /etc/default/locale
    echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale
    source /etc/default/locale
    locale-gen en_US.UTF-8
    dpkg-reconfigure locales
fi

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password password'
package mysql-server
service mysql restart

package git
package build-essential
package apache2
package php5
package php5-mysql
package php5-curl
package php5-gd

php-settings-update 'date.timezone' 'Asia/Omsk'

# Create a database if one doesn't already exist.
mysql-database-create 'rsproject'